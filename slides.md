# Présentation avec Reveal.js

<style>
#myColor {
  color: red;
  }
#left {
    margin: 10px 0 15px 20px;
    text-align: center;
    float: left;
    z-index:-10;
    width:48%;
    font-size: 0.85em;
    line-height: 1.5;
}
#right {
    margin: 10px 0 15px 0;
    float: right;
    text-align: center;
    z-index:-10;
    width:48%;
    font-size: 0.85em;
    line-height: 1.5;
}

</style>

--

# Chapitre1

--

# 1.1 Une slide verticale

* Item 1 <!-- .element: class="fragment" -->
* Item 2 <!-- .element: class="fragment" -->

--
# 1.2 Une autre slide verticale

<span id='myColor'>Texte coloré</span>

--
# 1.3 Une derniere slide verticale

Juste par exemple

---
# Chapitre2
1. Un projet template qui intègre reveal js dans une image docker : https://gitlab.com/formations-kgaut/template-reveal-js
2. Un sous projet, comme celui là, qui contient les slides et utilise l'image docker précédemment crée
3. La CI de ce projet publie la présentation automatiquement à chaque modification sur gitlab page.

--

# 2.1 Une slide verticale



--
# 2.2 Une autre slide verticale

Inserer une image: 

 ![texte sous l'image](images/jdriven.png)


Liens: 

 [titre du lien](https://formations-douxvalkyn.gitlab.io/presentation)


Citations: 

> Oh la belle prise !


Tableaux:

| Aligné à gauche  | Centré          | Aligné à droite |
| :--------------- |:---------------:| -----:|
| Aligné à gauche  |   ce texte        |  Aligné à droite |
| Aligné à gauche  | est             |   Aligné à droite |
| Aligné à gauche  | centré          |    Aligné à droite |


--
# 2.3 Une derniere slide verticale

---
# Chapitre3
1. Un projet template qui intègre reveal js dans une image docker : https://gitlab.com/formations-kgaut/template-reveal-js
2. Un sous projet, comme celui là, qui contient les slides et utilise l'image docker précédemment crée
3. La CI de ce projet publie la présentation automatiquement à chaque modification sur gitlab page.

--

# 3.1 Une slide verticale

Juste par exemple

--
# 3.2 Une autre slide verticale

Juste par exemple

--
# 3.3 Une derniere slide verticale