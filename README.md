# Presentation
Modèle pour visualiser une présentation avec Reveal.js. 
Reveal.js qui est une librairie qui permet de créer des présentations (à la Powerpoint ou Google Slides) en utilisant de l'HTML, du CSS et un peu de JavaScript.


## Organisation
Le projet Template contient une image Docker (avec reveal.js  et un .css commun) qui sera utilisée par toutes les présentations.
Les autres projets nécessitent 2 fichiers minimum:
- slides.md: contient le contenu de la présentation
- .gitlab-ci.yml: configuration de la CI qui va récupérer l'image précédement créée, copier le fichier slides.md, modifier le titre, puis publier la présentation sur gitlab pages; à chaque push, le site est immédiatement généré

## Ajouter un dossier
Pour ajouter un dossier au livrable généré, il faut ajouter une ligne dans le fichier .gitlab-ci.yml de la presentation. Par exemple pour ajouter un dossier ```images``` (avec tout son contenu):
```
- mv images public/images
```

## Visualiser le rendu
L'url est trouvable dans le menu Deploy/Pages, pour l'exemple c'est:
https://formations-douxvalkyn.gitlab.io/presentation

## Langage Markdown
Pour faire apparaitre des élements petit à petit:
```
* Item 1 <!-- .element: class="fragment" -->
* Item 2 <!-- .element: class="fragment" -->`
```
Titres: 
```
       # titre
       ## section niveau 2
       ### sous section niveau 3
```

Style de texte: 
```
       **mots en gras**
       *idem en italique*
       ~~barré~~
       ```bloc de code``` 
```

Couleurs:
```
<style>
#myColor {
  color: red;
}
</style>
<span id='myColor'>open</span>
```



Listes: 
```
       - liste 1
       - liste 2
           
       1. premier
       2. second
```
Inserer une image: 
```
 ![texte sous l'image](nom du fichier)
```  


Liens: 
```
 [titre du lien](url du lien)
```  

Citations: 
```
> Oh la belle prise !
```  

Tableaux:
``` 
| Aligné à gauche  | Centré          | Aligné à droite |
| :--------------- |:---------------:| -----:|
| Aligné à gauche  |   ce texte        |  Aligné à droite |
| Aligné à gauche  | est             |   Aligné à droite |
| Aligné à gauche  | centré          |    Aligné à droite |
``` 



## Documentation
Source: https://www.kgaut.net/blog/2022/deployer-ses-presentations-sur-gitlab-pages-avec-revealjs-docker-et-gitlab-ci.html